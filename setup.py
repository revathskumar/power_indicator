#!/usr/bin/env python
# -*- coding: utf-8 -*-

# pragma: nocover
import io
import sys

import setuptools
from distutils.core import setup
version='1.0.0'


setup(
    name='powerindicator',
    version=version,
    install_requires=[
        'python_version > "3.5"',
    ],
    long_description=io.open('README.md', encoding='utf8').read(),
    url='https://gitlab.com/revathskumar/power-indicator',
    license='GPL-3',
    author='Revath S Kumar',
    author_email='rsk@revathskumar.com',
    description='Power indicator',
	py_modules=['powerindicator'],
    # scripts=['powerindicator'],
	entry_points={
        'console_scripts': [
            'powerindicator = powerindicator:main'
        ]
    },
)
