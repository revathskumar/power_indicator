#!/usr/bin/env python
import os
import signal
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Gtk as gtk, Gio
from gi.repository import AppIndicator3 as appindicator

APPINDICATOR_ID = 'Power Indicator'

# https://lazka.github.io/pgi-docs/#AppIndicator3-0.1/classes/Indicator.html#AppIndicator3.Indicator.set_icon_full

indicator = None

def main():
    global indicator
    indicator = appindicator.Indicator.new(APPINDICATOR_ID, os.path.abspath('power-cord.svg'), appindicator.IndicatorCategory.APPLICATION_STATUS)
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
    indicator.set_menu(build_menu())
    # indicator.set_label("Power", "A")
    print()
    gtk.main()


def build_menu():
    menu = gtk.Menu()

    menu_item = gtk.CheckMenuItem.new_with_label('Auto Suspend')
    auto_suspend = Gio.Settings.new('org.gnome.settings-daemon.plugins.power').get_int('sleep-inactive-ac-timeout')
    print(auto_suspend)
    print(auto_suspend == 0)
    if auto_suspend == 0:
        menu_item.set_active(False)
    else:
        menu_item.set_active(True)
    menu_item.connect('activate', toggle)
    menu.append(menu_item)

    menu.append(gtk.SeparatorMenuItem())

    quit_menu_item = gtk.MenuItem('Quit')
    quit_menu_item.connect('activate', quit)
    menu.append(quit_menu_item)

    menu.show_all()
    return menu

def quit(_):
    gtk.main_quit()

def toggle(menu_item):
    print('toggle')
    if menu_item.props.active:
        Gio.Settings.new('org.gnome.settings-daemon.plugins.power').set_int('sleep-inactive-ac-timeout', 900)
    else:
        Gio.Settings.new('org.gnome.settings-daemon.plugins.power').set_int('sleep-inactive-ac-timeout', 0)


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)

main()
